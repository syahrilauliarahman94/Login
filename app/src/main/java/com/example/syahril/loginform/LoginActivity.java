package com.example.syahril.loginform;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    EditText etUsername;
    EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void Login(View v) {
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);

        String Username = etUsername.getText().toString();
        String Password = etPassword.getText().toString();

        Toast.makeText(getApplicationContext(),
                "Usename : " +Username+"\n"+
                        "Password :" +Password,
                Toast.LENGTH_SHORT).show();
    }
}